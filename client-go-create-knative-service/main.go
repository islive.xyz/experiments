package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	knsrvv1 "knative.dev/serving/pkg/apis/serving/v1"
	servingv1client "knative.dev/serving/pkg/client/clientset/versioned/typed/serving/v1"
	"sigs.k8s.io/yaml"
)

func main() {
	svc := knsrvv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: "coolservice",
		},
		Spec: knsrvv1.ServiceSpec{
			ConfigurationSpec: knsrvv1.ConfigurationSpec{
				Template: knsrvv1.RevisionTemplateSpec{
					Spec: knsrvv1.RevisionSpec{
						PodSpec: v1.PodSpec{
							Containers: []v1.Container{{
								Name:  "coolservice",
								Image: "nginx:stable",
								Ports: []v1.ContainerPort{{
									ContainerPort: int32(80),
								}},
							}},
						},
					},
				},
			},
		},
	}
	svcYAML, err := yaml.Marshal(svc)
	if err != nil {
		log.Println("error:", err)
	}
	log.Println("Service definition")
	fmt.Println(string(svcYAML))

	var config *rest.Config
	if _, err := os.Stat("/var/run/secrets/kubernetes.io/serviceaccount"); os.IsExist(err) {
		config, err = rest.InClusterConfig()
		if err != nil {
			panic(err.Error())
		}
	} else {
		home := homedir.HomeDir()
		config, err = clientcmd.BuildConfigFromFlags("", filepath.Join(home, ".kube", "config"))
		if err != nil {
			panic(err.Error())
		}
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	knclient, err := servingv1client.NewForConfig(config)
	if err != nil {
		panic(err)
	}
	_ = clientset
	_ = knclient

	servicesList, err := knclient.Services("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println("Installed Services")
	for _, svc := range servicesList.Items {
		fmt.Printf("- %v/%v\n", svc.ObjectMeta.Namespace, svc.ObjectMeta.Name)
	}
	deploymentsList, err := clientset.AppsV1().Deployments("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println("Installed Deployments")
	for _, dpl := range deploymentsList.Items {
		fmt.Printf("- %v/%v\n", dpl.ObjectMeta.Namespace, dpl.ObjectMeta.Name)
	}

	log.Println("Creating Service")
	_, err = knclient.Services("default").Create(context.TODO(), &svc, metav1.CreateOptions{})
	if err != nil {
		panic(err)
	}

	log.Println("Waiting for Service to be ready")
poll:
	for {
		log.Printf("... waiting\n")
		time.Sleep(2 * time.Second)
		svc, err := knclient.Services("default").Get(context.TODO(), svc.ObjectMeta.Name, metav1.GetOptions{})
		if err != nil {
			continue
		}
		log.Println("!! Found", svc.ObjectMeta.Name)
		for _, c := range svc.Status.Conditions {
			if c.Type == "Ready" && c.Status == "True" {
				break poll
			}
		}
		log.Println("... conditions not 'Ready' yet")
	}

	liveService, err := knclient.Services("default").Get(context.TODO(), svc.ObjectMeta.Name, metav1.GetOptions{})
	if err != nil {
		panic(err)
	}
	log.Println("Attempting to request from Service")
	res, err := http.Get(liveService.Status.URL.String())
	if err != nil {
		panic(err)
	}
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(resBody))

	servicesList, err = knclient.Services("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println("Installed Services")
	for _, svc := range servicesList.Items {
		fmt.Printf("- %v/%v\n", svc.ObjectMeta.Namespace, svc.ObjectMeta.Name)
	}
	deploymentsList, err = clientset.AppsV1().Deployments("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println("Installed Deployments")
	for _, dpl := range deploymentsList.Items {
		fmt.Printf("- %v/%v\n", dpl.ObjectMeta.Namespace, dpl.ObjectMeta.Name)
	}

	log.Println("Deleting Service")
	err = knclient.Services("default").Delete(context.TODO(), svc.ObjectMeta.Name, metav1.DeleteOptions{})
	if err != nil {
		panic(err)
	}
	log.Println("Complete")
}
