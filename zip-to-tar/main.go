package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"io"
	"log"
	"os"
	"path"
)

const TarFolderPrefix = "/var/run/ko"

type ZipToTarOptions struct {
	FolderPrefix string
}

func ZipToTar(input string, output string, options *ZipToTarOptions) error {
	zipFile, err := zip.OpenReader(input)
	if err != nil {
		return err
	}
	defer zipFile.Close()

	newFile, err := os.Create(output)
	if err != nil {
		return err
	}
	defer newFile.Close()
	tw := tar.NewWriter(newFile)
	for _, f := range zipFile.File {
		log.Println(f.Name)
		var buf bytes.Buffer
		rc, err := f.Open()
		if err != nil {
			return err
		}
		info := f.FileInfo()
		if err != nil {
			return err
		}
		if info.IsDir() {
			continue
		}
		_, err = io.Copy(&buf, rc)
		if err != nil {
			return err
		}
		defer rc.Close()

		hdr := &tar.Header{
			Name: path.Join(options.FolderPrefix, f.Name),
			Mode: 0444,
			Size: int64(buf.Len()),
		}
		if err := tw.WriteHeader(hdr); err != nil {
			return err
		}
		if _, err := tw.Write(buf.Bytes()); err != nil {
			return err
		}
	}
	if err := tw.Close(); err != nil {
		return err
	}
	return nil
}

// TODO func to output to buffer

func main() {
	log.Println("Converting site.zip into site.tar")
	if err := ZipToTar("site.zip", "site.tar", &ZipToTarOptions{FolderPrefix: TarFolderPrefix}); err != nil {
		log.Fatal(err)
	}
	log.Println("Completed")
}
