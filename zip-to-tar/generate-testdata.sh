#!/usr/bin/env bash

cd "$(dirname "$(realpath "${0}")")"

if [ -f ./site.zip ]; then
  echo "file 'site.zip' already exists"
  exit 0
fi

WORKDIR="$(mktemp -d)"
(
  cd "${WORKDIR}"
  mkdir page1/
  cat <<EOF | tee index.html
<!DOCTYPE HTML>
<html>
  <head>
    <title>Cool site</title>
    <link href="./style.css" rel="stylesheet">
  </head>
  <body>
    <h1>Hello</h1>
    <p>This site? It ain't bad</p>
  </body>
</html>
EOF
  cat <<EOF | tee style.css
body {
  background: #003000;
}
h1, p {
  color: white;
}
EOF
  cat <<EOF | tee page1/index.html
<!DOCTYPE HTML>
<html>
  <head>
    <title>Page 1!</title>
    <link href="./style.css" rel="stylesheet">
  </head>
  <body>
    <h1>Page 1</h1>
    <p>Pageeeee oneeeee</p>
  </body>
</html>
EOF
  zip -r site.zip *
  # zip site.zip index.html style.css page1
)

cp "${WORKDIR}/site.zip" ./site.zip
