package main

import (
	"bytes"
	"context"
	"io/ioutil"
	"log"

	minio "github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type FileAccess struct {
	Client     *minio.Client
	BucketName string
}

// Get ...
// retrieves a given object
func (f FileAccess) Get(name string) (objectBytes []byte, objectInfo minio.ObjectInfo, err error) {
	object, err := f.Client.GetObject(context.TODO(), f.BucketName, name, minio.GetObjectOptions{})
	if err != nil {
		log.Printf("%#v\n", err)
		return []byte{}, minio.ObjectInfo{}, err
	}
	defer object.Close()
	objectInfo, err = object.Stat()
	if err != nil {
		log.Printf("%#v\n", err)
		return []byte{}, minio.ObjectInfo{}, err
	}
	objectBytes, err = ioutil.ReadAll(object)
	if err != nil {
		log.Printf("%#v\n", err)
		return []byte{}, minio.ObjectInfo{}, err
	}
	return objectBytes, objectInfo, err
}

// Put ...
// uploads a file
func (f FileAccess) Put(name string, data []byte) error {
	reader := bytes.NewReader(data)
	info, err := f.Client.PutObject(context.TODO(), f.BucketName, name, reader, int64(reader.Len()), minio.PutObjectOptions{})
	if err != nil {
		return err
	}
	log.Printf("Sucessful uploaded '%v' into bucket\n", info.Key)
	return nil
}

// CreateBucket ...
// creates a bucket
func (f FileAccess) CreateBucket() error {
	err := f.Client.MakeBucket(context.TODO(), f.BucketName, minio.MakeBucketOptions{})
	if err != nil {
		return err
	}
	log.Printf("Sucessfully created bucket '%v'\n", f.BucketName)
	return nil
}

// ListBucket ...
// lists all the buckets
func (f FileAccess) ListBuckets() (list []string, err error) {
	bucketList, err := f.Client.ListBuckets(context.TODO())
	if err != nil {
		return []string{}, err
	}
	for _, bucket := range bucketList {
		list = append(list, bucket.Name)
	}
	return list, nil
}

// EnsureBucketExists ...
// creates a bucket
func (f FileAccess) EnsureBucketExists() error {
	bucketExists := false
	list, err := f.ListBuckets()
	if err != nil {
		return err
	}
	for _, name := range list {
		if name == f.BucketName {
			bucketExists = true
		}
	}
	if !bucketExists {
		err = f.CreateBucket()
		if err != nil {
			return err
		}
	}
	return nil
}

// Open ...
// open a Minio client
func Open(endpoint string, accessKey string, secretKey string, bucketName string, useSSL bool) (FileAccess, error) {
	mc, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKey, secretKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		return FileAccess{}, err
	}
	return FileAccess{Client: mc, BucketName: bucketName}, err
}

func main() {
	log.Println("Connecting")
	o, err := Open("127.0.0.1:9000", "minioadmin", "minioadmin", "object-store-upload-example", false)
	if err != nil {
		log.Panic(err)
	}
	err = o.EnsureBucketExists()
	if err != nil {
		log.Panic(err)
	}
	log.Println("Uploading")
	err = o.Put("coolthing", []byte("you"))
	if err != nil {
		log.Panic(err)
	}
	log.Println("Complete")

	log.Println("Downloading the file")
	fileBytes, fileInfo, err := o.Get("coolthing")
	if err != nil {
		log.Panic(err)
	}
	log.Printf("%+v", fileInfo)
	log.Println(string(fileBytes))
}
