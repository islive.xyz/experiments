#!/usr/bin/env bash

IMAGE_MINIO=minio/minio:RELEASE.2023-02-27T18-10-45Z

podman run -it --rm -e TZ=Pacific/Auckland -p 9000:9000 -p 9001:9001 ${IMAGE_MINIO} server /data --console-address ":9001"
