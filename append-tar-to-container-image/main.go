package appendtartocontainerimage

import (
	"fmt"
	"log"

	"github.com/google/go-containerregistry/pkg/crane"
	"github.com/google/go-containerregistry/pkg/name"
	v1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	specsv1 "github.com/opencontainers/image-spec/specs-go/v1"
)

// TODO multi-arch
var (
	// linux/amd64
	// DefaultBaseImageRef = "registry.gitlab.com/bobymcbobs/go-http-server:1.5.0@sha256:4a3419326d4a1d4169c0e20c7e363ae417581a76cfaee1ce95d78f9fed6ad3fa"
	// linux/arm64
	DefaultBaseImageRef = "registry.gitlab.com/bobymcbobs/go-http-server:1.5.0@sha256:2c79c8e596152c1d12256148bd578dda37437f8df7d94595557b339951302bca"
	DefaultImageRef     = "docker.io/bobymcbobs/anything:site-deployer"
)

type SiteBuilderOptions struct {
	Push     bool
	Save     bool
	ImageRef string
}

type SiteBuilder struct {
	BaseImageRef string
	Options      *SiteBuilderOptions
}

func NewSiteBuilder(o *SiteBuilderOptions) *SiteBuilder {
	if o.ImageRef == "" {
		o.ImageRef = DefaultImageRef
	}
	return &SiteBuilder{
		BaseImageRef: DefaultBaseImageRef,
		Options:      o,
	}
}

func (s *SiteBuilder) AppendSite(inputTarPath string, outputTarPath string) (imageRefWithManifest string, err error) {
	log.Printf("Pulling image '%v'\n", s.BaseImageRef)
	base, err := crane.Pull(s.BaseImageRef)
	if err != nil {
		return "", err
	}
	layerCount, err := base.Layers()
	if err != nil {
		return "", err
	}
	log.Printf("Base image has %v layers", len(layerCount))
	log.Println("Appending site.tar")
	img, err := crane.Append(base, inputTarPath)
	if err != nil {
		return "", err
	}
	layerCount, err = img.Layers()
	if err != nil {
		return "", err
	}
	log.Printf("Image has %v layers", len(layerCount))
	log.Println("Annotating image")
	ref, err := name.ParseReference(s.BaseImageRef)
	if err != nil {
		return "", fmt.Errorf("parsing ref %q: %w", s.BaseImageRef, err)
	}
	baseDigest, err := base.Digest()
	if err != nil {
		return "", err
	}
	anns := map[string]string{
		specsv1.AnnotationBaseImageDigest: baseDigest.String(),
	}
	if _, ok := ref.(name.Tag); ok {
		anns[specsv1.AnnotationBaseImageName] = ref.Name()
	}
	img = mutate.Annotations(img, anns).(v1.Image)

	if s.Options.Push {
		log.Printf("Pushing to '%v'\n", s.Options.ImageRef)
		err = crane.Push(img, s.Options.ImageRef)
		if err != nil {
			return "", err
		}
		sum, err := crane.Digest(s.Options.ImageRef)
		if err != nil {
			return "", err
		}
		imageRefWithManifest = fmt.Sprintf("%v@%v", s.Options.ImageRef, string(sum))
	}
	if s.Options.Save {
		log.Printf("Saving to '%v'\n", outputTarPath)
		err = crane.Save(img, s.Options.ImageRef, outputTarPath)
		if err != nil {
			return "", err
		}
	}
	log.Println("Complete")
	return imageRefWithManifest, nil
}

func main() {
	_, err := NewSiteBuilder(&SiteBuilderOptions{Push: false, Save: true}).AppendSite("../zip-to-tar/site.tar", "./site-container-image.tar")
	if err != nil {
		log.Panic(err)
	}
}
