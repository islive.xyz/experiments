package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	a "gitlab.com/islive.xyz/experiments/append-tar-to-container-image"
	z "gitlab.com/islive.xyz/experiments/zip-to-tar"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	knsrvv1 "knative.dev/serving/pkg/apis/serving/v1"
	servingv1client "knative.dev/serving/pkg/client/clientset/versioned/typed/serving/v1"
	"sigs.k8s.io/yaml"
)

// TODO
// - image destination
// - push or save local tar option
// - inputs for site
// - multi-arch images (otherwise developing is difficult)

var (
	svc = knsrvv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: "site-deploy-test-",
			Labels: map[string]string{
				"site-deploy-managed": "true",
			},
		},
		Spec: knsrvv1.ServiceSpec{
			ConfigurationSpec: knsrvv1.ConfigurationSpec{
				Template: knsrvv1.RevisionTemplateSpec{
					Spec: knsrvv1.RevisionSpec{
						PodSpec: v1.PodSpec{
							Containers: []v1.Container{{
								Name:  "site-deploy",
								Image: "",
								Ports: []v1.ContainerPort{{
									ContainerPort: int32(8080),
								}},
							}},
						},
					},
				},
			},
		},
	}
)

func main() {
	var config *rest.Config
	if _, err := os.Stat("/var/run/secrets/kubernetes.io/serviceaccount"); os.IsExist(err) {
		config, err = rest.InClusterConfig()
		if err != nil {
			panic(err.Error())
		}
	} else {
		home := homedir.HomeDir()
		config, err = clientcmd.BuildConfigFromFlags("", filepath.Join(home, ".kube", "config"))
		if err != nil {
			panic(err.Error())
		}
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	_ = clientset
	knclient, err := servingv1client.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	log.Println("Taring zip")
	if err := z.ZipToTar("./site.zip", "./site.tar", &z.ZipToTarOptions{FolderPrefix: z.TarFolderPrefix}); err != nil {
		log.Panic(err)
	}
	log.Println("Appending site layer")
	imageRef, err := a.NewSiteBuilder(&a.SiteBuilderOptions{Push: true, Save: true, ImageRef: a.DefaultImageRef}).AppendSite("./site.tar", "./site-container-image.tar")
	if err != nil {
		log.Panic(err)
	}
	svc.Spec.ConfigurationSpec.Template.Spec.PodSpec.Containers[0].Image = imageRef
	svcYAML, err := yaml.Marshal(svc)
	if err != nil {
		log.Println("error:", err)
	}
	log.Println("Service definition")
	fmt.Println(string(svcYAML))

	log.Println("Creating Service")
	createdService, err := knclient.Services("default").Create(context.TODO(), &svc, metav1.CreateOptions{})
	if err != nil {
		panic(err)
	}
poll:
	for {
		log.Printf("... waiting\n")
		time.Sleep(2 * time.Second)
		svc, err := knclient.Services("default").Get(context.TODO(), createdService.ObjectMeta.Name, metav1.GetOptions{})
		if err != nil {
			continue
		}
		log.Println("!! Found", svc.ObjectMeta.Name)
		for _, c := range svc.Status.Conditions {
			if c.Type == "Ready" && c.Status == "True" {
				break poll
			}
		}
		log.Println("... conditions not 'Ready' yet")
	}

	liveService, err := knclient.Services("default").Get(context.TODO(), createdService.ObjectMeta.Name, metav1.GetOptions{})
	if err != nil {
		panic(err)
	}
	log.Println("Service accessible from:", liveService.Status.URL.String())
	log.Println("Done")
}
