#!/usr/bin/env bash

cd "$(dirname "$(realpath "${0}")")"

if [ -f ./site.zip ]; then
  echo "file 'site.zip' already exists"
  exit 0
fi

WORKDIR="$(mktemp -d)"
(
  cd "${WORKDIR}"
  cat <<EOF | tee index.html
<!DOCTYPE HTML>
<html>
  <head>
    <title>Cool site</title>
    <link href="./style.css" rel="stylesheet">
  </head>
  <body>
    <h1>Hello</h1>
    <p>Pushing straight to prod</p>
    <p>Woooohoooooo!</p>
  </body>
</html>
EOF
  cat <<EOF | tee style.css
body {
  background: #003000;
}
h1, p {
  color: white;
}
EOF
  zip site.zip index.html style.css
)

cp "${WORKDIR}/site.zip" ./site.zip
