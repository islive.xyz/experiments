package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/miekg/dns"
)

func main() {
	config, _ := dns.ClientConfigFromFile("/etc/resolv.conf")
	c := dns.Client{}
	m := dns.Msg{}
	m.SetQuestion("calebwoodbine.com.", dns.TypeA)
	r, _, err := c.Exchange(&m, net.JoinHostPort(config.Servers[0], config.Port))
	if err != nil {
		return
	}
	if r.Rcode != dns.RcodeSuccess {
		return
	}
	addr := ""
	for _, a := range r.Answer {
		if record, ok := a.(*dns.A); ok {
			fmt.Printf("%+v\n", record)
			fmt.Printf("%s\n", record.String())
			parts := strings.Split(record.String(), "\t")
			fmt.Println(len(parts))
			addr = parts[len(parts)-1]
		}
	}
	fmt.Printf("%v\n", addr)
}
